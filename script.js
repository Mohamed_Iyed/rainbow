let hexColor = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];    
let bodyChilds = [...document.querySelectorAll('body span')];
let createspan = () => {
    if(bodyChilds.length !== 0){
        bodyChilds.array.forEach(element => {
            document.body.removeChild(element);
        });
    }
    let str = '';
    for(let i = 0; i < 6; i++){
        str += hexColor[Math.floor(Math.random() * hexColor.length)];
    }
    let span = document.createElement('span');
    span.classList.add('bound');
    span.style.backgroundColor = `#${str}`;
    document.body.appendChild(span);
}
setInterval(createspan, 500);
